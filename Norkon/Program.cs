﻿
public class Program
{
    public static async Task Main()
    {
        var builder = new HostBuilder()
            .ConfigureAppConfiguration((hostContext, config) =>
            {
                config.AddJsonFile("appsettings.json");
            })
            .ConfigureServices((hostContext, services) =>
            {
                services.AddOptions();
                services.Configure<WebSocketOptions>(hostContext.Configuration.GetSection("WebSocketOptions"));
                services.AddSingleton<ClientWebSocket>();
                services.AddSingleton<INorkonConnectionSource, WebSocketNorkonConnectionSource>();
                services.AddSingleton<ConnectionMonitor>();
            })
            .UseConsoleLifetime();

        using var host = builder.Build();
        var connectionMonitor = host.Services.GetRequiredService<ConnectionMonitor>();

        await foreach (var updateInfo in connectionMonitor.MonitorConnections(CancellationToken.None))
        {
            Console.WriteLine($"[{updateInfo.TimeStamp}] Update -{updateInfo.UpdateNumber}: {updateInfo.NumberOfConnections} connections");
        }

        await host.RunAsync();
    }
}	