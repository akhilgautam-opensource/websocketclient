﻿
namespace Norkon
{
    //public class WebSocketNorkonConnectionSource : INorkonConnectionSource
    //{
    //    private readonly ClientWebSocket _webSocket;
    //    private readonly string webSocketEndpoint;


    //    public WebSocketNorkonConnectionSource(string webSocket)
    //    {
    //        _webSocket = webSocketEndpoint;
    //    }
    //    public async IAsyncEnumerable<int> GetConnectionCountAsync(CancellationToken cancellationToken)
    //    {
    //        var uri = new Uri("ws://localhost:5000");
    //        await _webSocket.ConnectAsync(uri, cancellationToken);
    //        var buffer = new byte[1024];
    //        var receiveBuffer = new ArraySegment<byte>(buffer);

    //        while (!cancellationToken.IsCancellationRequested && _webSocket.State == WebSocketState.Open)
    //        {
    //            var result = await _webSocket.ReceiveAsync(receiveBuffer, cancellationToken);
    //            if (result.MessageType == WebSocketMessageType.Text)
    //            {
    //                var message = System.Text.Encoding.UTF8.GetString(receiveBuffer.Array, 0, result.Count);
    //                if (int.TryParse(message, out var connectionCount))
    //                {
    //                    yield return connectionCount;
    //                }
    //            }
    //        }
    //    }
    //}

    public class WebSocketNorkonConnectionSource : INorkonConnectionSource
    {
        private readonly string webSocketEndpoint;

        public WebSocketNorkonConnectionSource(string webSocketEndpoint)
        {
            this.webSocketEndpoint = webSocketEndpoint;
        }
        async IAsyncEnumerable<int> INorkonConnectionSource.GetConnectionCountAsync(CancellationToken cancellationToken)
        {
            using var clientWebSocket = new ClientWebSocket();

            await clientWebSocket.ConnectAsync(new Uri(webSocketEndpoint), cancellationToken);

                while (!cancellationToken.IsCancellationRequested)
                {
                    var buffer = new byte[1024];
                    var segment = new ArraySegment<byte>(buffer);
                    var result = await clientWebSocket.ReceiveAsync(segment, cancellationToken);

                    if (result.MessageType == WebSocketMessageType.Text && result.EndOfMessage)
                    {
                        var message = System.Text.Encoding.UTF8.GetString(buffer, 0, result.Count);
                        if (int.TryParse(message, out var connections))
                        {
                            yield return connections;
                        }
                    }
                }
            }
           
        }
    }


