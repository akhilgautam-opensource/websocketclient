﻿namespace Norkon
{
    public class ConnectionMonitor
    {
        private readonly INorkonConnectionSource _connectionSource;

        public ConnectionMonitor(INorkonConnectionSource connectionSource)
        {
            _connectionSource = connectionSource;
        }

        public async IAsyncEnumerable<UpdateInfo> MonitorConnections(CancellationToken cancellationToken)
        {
            var updateNumber = 0;

            await foreach (var connectionCount in _connectionSource.GetConnectionCountAsync(cancellationToken))
            {
                yield return new UpdateInfo(DateTime.Now, connectionCount, updateNumber);
                updateNumber++;
            }
        }
    }
}
