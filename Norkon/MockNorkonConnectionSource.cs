﻿namespace Norkon
{
    /// <summary>
    /// 
    /// </summary>
    public class MockNorkonConnectionSource : INorkonConnectionSource
    {
       
        public async IAsyncEnumerable<int> GetConnectionCountAsync(CancellationToken cancellationToken)
        {
            var random = new Random();
            var updateNumber = 0;

            while (!cancellationToken.IsCancellationRequested)
            {
                var connectionCount = random.Next(0, 101); // Generate a random number between 0 and 100
                yield return connectionCount;

                updateNumber++;
                await Task.Delay(500, cancellationToken); // Wait for 500ms before generating the next update
            }
        }
    }
}

public record UpdateInfo(DateTime TimeStamp, int NumberOfConnections, int UpdateNumber);
