﻿namespace Norkon
{
    public class WebSocketOptions
    {
        /// <summary>
        /// Endpoint for websocket server
        /// </summary>
        public string Endpoint { get; private set; }
    }

}
