﻿namespace Norkon
{
    /// <summary>
    /// 
    /// </summary>
    public interface INorkonConnectionSource
    {
        IAsyncEnumerable<int> GetConnectionCountAsync(CancellationToken cancellationToken);
    }
}