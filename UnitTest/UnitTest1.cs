using Norkon;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;
using System.Linq;


namespace YourTestProjectNamespace
{
    public class ConnectionMonitorTests
    {
        [Fact]
        public async Task MonitorConnections_Should_Return_UpdateInfos()
        {
            // Arrange
            var mockConnectionSource = new MockNorkonConnectionSource();
            var connectionMonitor = new ConnectionMonitor(mockConnectionSource);
            var expectedCount = 10;

            // Act
            var updateInfos = new List<UpdateInfo>();
            await foreach (var updateInfo in connectionMonitor.MonitorConnections(CancellationToken.None))
            {
                updateInfos.Add(updateInfo);

                if (updateInfos.Count >= expectedCount)
                    break;
            }
            // Assert
            Assert.Equal(expectedCount, updateInfos.Count);
        }
    }
}
