

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Norkon_UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        private const string TestWebSocketEndpoint = "wss://test-norkon.ssssss.net/Pulse/"; 

        [TestMethod]
        public async Task TestWebSocketNorkonConnectionSource()
        {
            // Arrange
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false) 
                .Build();

            var serviceProvider = new ServiceCollection()
                .Configure<TestWebSocketConfig>(configuration.GetSection("WebSocketEndpoint"))
                .AddTransient<INorkonConnectionSource, WebSocketNorkonConnectionSource>(provider =>
                {
                    var options = provider.GetRequiredService<IOptions<TestWebSocketConfig>>();
                    return new WebSocketNorkonConnectionSource(options.Value.WebSocketEndpoint);
                })
                .AddTransient<ConnectionMonitor>() 
                .BuildServiceProvider();

            var cancellationTokenSource = new CancellationTokenSource();
            var connectionMonitor = serviceProvider.GetRequiredService<ConnectionMonitor>(); 

            // Act & Assert
            var timeoutTask = Task.Delay(TimeSpan.FromSeconds(5));
            await foreach (var connections in connectionMonitor.MonitorConnections(cancellationTokenSource.Token).WithCancellation(cancellationTokenSource.Token))
            {
                // This test will keep waiting until it receives an update or the timeout expires (5 seconds).
                Assert.IsTrue(connections.NumberOfConnections >= 0, "Received a valid number of connections");
                cancellationTokenSource.Cancel(); 
                break;
            }

            await timeoutTask; 
        
        }

        [TestMethod]
        public async Task TestNorkonConnectionMonitor()
        {
            // Arrange
            using var serviceProvider = new ServiceCollection()
                .AddTransient<INorkonConnectionSource, MockNorkonConnectionSource>()
                .AddTransient<ConnectionMonitor>()
                .BuildServiceProvider();

            var cancellationTokenSource = new CancellationTokenSource();
            var connectionMonitor = serviceProvider.GetRequiredService<ConnectionMonitor>();

            // Act & Assert
            var timeoutTask = Task.Delay(TimeSpan.FromSeconds(5));
            var updatesCount = 0;
            await foreach (var updateInfo in connectionMonitor.MonitorConnections(cancellationTokenSource.Token).WithCancellation(cancellationTokenSource.Token))
            {
                // This test will wait for 10 seconds and check if it receives at least one update.
                Assert.IsTrue(updateInfo.NumberOfConnections >= 0, "Received a valid number of connections");
                updatesCount++;
                if (updatesCount >= 5) // Stop the test after receiving 5 updates
                    break;
            }
            await timeoutTask;
        }
    }

}

